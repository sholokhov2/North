#Task_1
# Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

vowels = 'aeiouAEIOU'
string = "Ua, IO, Ai, eu"
words = string.split()
count = 0

for word in words:
    for i in range(len(word) - 1):
        if word[i] in vowels and word[i + 1] in vowels:
            count += 1
            break

print("Кількість слів з двома голосними літерами підряд:", count)



#Task_2
# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
# "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:

lower_limit = 35.9
upper_limit = 37.339

shops = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "store": 37.720,
    "rozetka": 38.003
}

match_shops = []

for shop, price in shops.items():
    if lower_limit <= price <= upper_limit:
        match_shops.append(shop)

if match_shops:
    print("Match:", ", ".join(match_shops))
else:
    print("Співпадінь не знайдено.")