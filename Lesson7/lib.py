print('.' * 50)
print('                     Task №7')
print('.' * 50)

import time

def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Час виконання функції {func.__name__}: {execution_time:.6f} секунд")
        return result
    return wrapper

def get_age_input():
    while True:
        try:
            age = int(input("Введіть свій вік: "))
            if age < 0:
                print("Вік не може бути від'ємним числом. Спробуйте ще раз.")
                continue
            return age
        except ValueError:
            print("Введено некоректне значення. Будь ласка, введіть число.")

def main():
    age = get_age_input()

    if age < 7:
        print(f"Тобі ж {age} {'рік' if age == 1 else 'роки'}! Де твої батьки?")
    elif age < 16:
        print(f"Тобі лише {age} {'рік' if age == 1 else 'роки'}, а це е фільм для дорослих!")
    elif age > 65:
        print(f"Вам {age} {'рік' if age == 1 else 'роки'}? Покажіть пенсійне посвідчення!")
    elif '7' in str(age):
        print(f"Вам {age} {'рік' if age == 1 else 'роки'}, вам пощастить")
    else:
        print(f"Незважаючи на те, що вам {age} {'рік' if age == 1 else 'роки'}, білетів всеодно нема!")