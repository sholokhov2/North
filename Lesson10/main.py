# Напишіть гру "rock-paper-scissors-lizard-Spock", посилання на правила.
# Ввід даних гравцем - через input
# Рекомендую подумати над такими класами як Player, GameFigure, Game.
# Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.


import random


class Player:
    def __init__(self, name):
        self.name = name
        self.choice = None

    def choose(self):
        print(f"{self.name}, выберите ваш вариант:")
        print("1. Rock")
        print("2. Paper")
        print("3. Scissors")
        print("4. Lizard")
        print("5. Spock")
        choice = int(input("Ваш выбор (1/2/3/4/5): "))
        while choice not in [1, 2, 3, 4, 5]:
            print("Неверный выбор. Попробуйте снова.")
            choice = int(input("Ваш выбор (1/2/3/4/5): "))

        self.choice = choice


class Game:
    def __init__(self):
        self.rules = {
            1: [3, 4],
            2: [1, 5],
            3: [2, 4],
            4: [2, 5],
            5: [1, 3]
        }

    def winner(self, choice1, choice2):
        if choice1 == choice2:
            return "Ничья"
        elif choice2 in self.rules[choice1]:
            return "Игрок 1 победил"
        else:
            return "Игрок 2 победил"

    def play(self):
        player1 = Player(input("Имя игрока 1: "))
        player2 = Player(input("Имя игрока 2: "))

        player1.choose()
        player2.choose()

        result = self.winner(player1.choice, player2.choice)
        print(result)


if __name__ == "__main__":
    game = Game()
    game.play()