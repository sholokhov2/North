# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу int або float

print('*' * 50)
print('*' * 50)
print('*' * 50)

class Point:
    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self._x = x
            self._y = y
        else:
            raise TypeError("x і y мають бути типу int або float")

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __str__(self):
        return f'Point [{self.x}:{self.y}]'

p1 = Point(1, 1.1)
p2 = Point(5, 5.5)

print(p1)
print(p2)

# Створіть класс Triangle (трикутник), який задається (ініціалізується) трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника.
# Для обчислень можна використати формулу Герона (https://en.wikipedia.org/wiki/Heron%27s_formula)


class Triangle:
    def __init__(self, point1, point2, point3):
        if isinstance(point1, Point) and isinstance(point2, Point) and isinstance(point3, Point):
            self.point1 = point1
            self.point2 = point2
            self.point3 = point3
        else:
            raise TypeError("Усі точки мають бути класу Point")

    @property
    def side1(self):
        return ((self.point2.x - self.point1.x) ** 2 + (self.point2.y - self.point1.y) ** 2) ** 0.5

    @property
    def side2(self):
        return ((self.point3.x - self.point2.x) ** 2 + (self.point3.y - self.point2.y) ** 2) ** 0.5

    @property
    def side3(self):
        return ((self.point1.x - self.point3.x) ** 2 + (self.point1.y - self.point3.y) ** 2) ** 0.5

    def calculate(self):
        side = (self.side1 + self.side2 + self.side3) / 2
        area = (side * (side - self.side1) * (side - self.side2) * (side - self.side3)) ** 0.5
        return area


p1 = Point(0, 0)
p2 = Point(3, 0)
p3 = Point(0, 4)

triangle = Triangle(p1, p2, p3)
area = triangle.calculate()
print(f"Площа трикутника: {area}")

