print("." * 50)
print("Task №3")
print("." * 50)

def format_age(age):
    if age % 10 == 1 and age % 100 != 11:
        return "рік"
    elif age % 10 in [2, 3, 4] and age % 100 not in [12, 13, 14]:
        return "роки"
    else:
        return "років"

def check_age(age):
    if age < 7:
        return f"Тобі ж {age} {format_age(age)}! Де твої батьки?"
    elif age < 16:
        return f"Тобі лише {age} {format_age(age)}, а це фільм для дорослих!"
    elif age > 65 or age == 65:
        return f"Вам {age} {format_age(age)}? Покажіть пенсійне посвідчення!"
    elif "7" in str(age):
        return f"Вам {age} {format_age(age)}, вам щастить"
    else:
        return f"Незважаючи на те, що вам {age} {format_age(age)}, білетів всеодно нема!"

def check_age_65(age):
    if age == 65:
        return f"Вам {age} {format_age(age)}? Покажіть пенсійне посвідчення!"
    return None

def check_special_age_67_76(age):
    special_ages = [68, 70, 72, 74, 76]
    if age in special_ages:
        return "Це спеціальний вік!"
    return None

try:
    age = int(input("Введіть ваш вік: "))
    special_result_65 = check_age_65(age)
    if special_result_65:
        print(special_result_65)
    else:
        special_result_67_76 = check_special_age_67_76(age)
        if special_result_67_76:
            print(special_result_67_76)
        else:
            result = check_age(age)
            print(result)
except ValueError:
    print("Введіть коректне число для віку!")