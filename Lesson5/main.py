#Task_1 Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.

def convert_float(value):
    try:
        return float(value)
    except (ValueError, TypeError):
        return 0.0

print(convert_float(10))
print(convert_float("1.61"))
print(convert_float("Hello"))
print(convert_float(None))



#Task_2 Напишіть функцію, що приймає два аргументи. Функція повинна
# якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# у будь-якому іншому випадку повернути кортеж з цих аргументів

def multiply_numbers(arg1, arg2):
    return arg1 * arg2

def concatenate_str(arg1, arg2):
    return arg1 + arg2

def process_arguments(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        return multiply_numbers(arg1, arg2)
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return concatenate_str(arg1, arg2)
    else:
        return (arg1, arg2)

print(process_arguments(7, 7))
print(process_arguments("Hello, ", "world!"))
print(process_arguments(5.5, "abc"))
print(process_arguments([3, 5], {"x": 10}))



#Task_3 Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік.
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"

def format_age(age):
    if age % 10 == 1 and age % 100 != 11:
        return f"{age} рік"
    elif age % 10 in (2, 3, 4) and age % 100 not in (12, 13, 14):
        return f"{age} роки"
    else:
        return f"{age} років"

age = int(input("Введіть ваш вік: "))

if age < 7:
    print(f"Тобі ж {format_age(age)}! Де твої батьки?")
elif age == 7 or '7' in str(age):
    print(f"Вам {format_age(age)}, вам пощастить")
elif age < 16:
    print(f"Тобі лише {format_age(age)}, а це е фільм для дорослих!")
elif age > 65:
    print(f"Вам {format_age(age)}? Покажіть пенсійне посвідчення!")
else:
    print(f"Незважаючи на те, що вам {format_age(age)}, білетів всеодно нема!")
