# Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ),
# отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
#  "[дата, на яку актуальний курс]"
# 1. [назва валюти 1] to UAH: [значення курсу валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу валюти n]
# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс
#
# P.S. За можливості зробіть все за допомогою ООП
print('*' * 50)
print('*' * 50)
print('*' * 50)

import requests
from datetime import datetime


def get_exchange_rates(date):
    api_url = f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={date.strftime('%Y%m%d')}&json"
    response = requests.get(api_url)

    if response.status_code == 200:
        return response.json()
    else:
        raise Exception("Не вдалося отримати дані з API НБУ.")


def format_exchange_rates(rates):
    formatted_rates = [f"{rate['cc']} to UAH: {rate['rate']}" for rate in rates]
    return formatted_rates


def save_to_txt(formatted_rates, date):
    with open(f"exchange_rates_{date.strftime('%Y%m%d')}.txt", "w") as file:
        file.write(f"[дата, на яку актуальний курс]: {date.strftime('%Y-%m-%d')}\n")
        file.writelines([f"{i + 1}. {rate}\n" for i, rate in enumerate(formatted_rates)])


def main():
    user_date = input("Введіть дату у форматі YYYY-MM-DD (або залиште порожнім для отримання курсів за 15 вересня 2023 року): ")
    if not user_date:
        user_date = "2023-09-15"

    try:
        custom_date = datetime.strptime(user_date, '%Y-%m-%d')
        rates = get_exchange_rates(custom_date)
        formatted_rates = format_exchange_rates(rates)
        save_to_txt(formatted_rates, custom_date)
        print("Курси валют збережені у текстовий файл.")
    except ValueError:
        print("Некоректний формат дати.")
    except Exception as e:
        print(f"Помилка: {e}")


if __name__ == "__main__":
    main()
